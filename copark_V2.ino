#include <SoftwareSerial.h>
 
SoftwareSerial SIM900(7, 8);

String msg = String("");
int SmsContentFlag = 0;

// EN: Pin of the LED to turn ON and OFF depending on the received message
// FR: Pin de la LED a allumer/éteindre en fonction du message reçu
int ledPin = 13;

// Pin d'initialisation du GPRS au démarrage
int initPin = 9;

// EN: Code PIN of the SIM card (if applied)
// FR: Code PIN de la carte SIM (si applicable)
String SIM_PIN_CODE = String( "XXXX" );
 
void setup()
{
  SIM900.begin(19200);               // the GPRS baud rate   
  Serial.begin(19200);                 // the GPRS baud rate

  // Initialize la PIN
  pinMode( ledPin, OUTPUT ); 
  digitalWrite( ledPin, LOW ); 
  
  // Démarre GPRS
  pinMode( initPin, OUTPUT ); 
  digitalWrite( initPin, HIGH );
  delay(1000);
  digitalWrite( initPin, LOW );
  delay(4000);
  
  // Nécessité d'entré un PIN ?
  sendCmd("AT+CPIN?");
  //wait_for("OK",2000);  
  
  // Retourne la qualité du signal : +CSQ:<rssi>,<ber>
  sendCmd("AT+CSQ");
  //wait_for("OK",2000);  
  
  // Enregistré dans le réseau ?
  sendCmd("AT+CREG?");
  //wait_for("OK",2000);
  
  // Etat de la connection GPRS ?
  sendCmd("AT+CGATT?");
  //wait_for("OK",2000);
  
  // Etat de la connection multi-IP ?
  sendCmd("AT+CIPMUX=?");
  //wait_for("OK",2000);
  
  // Affiche le nom de l'opérateur réseau
  sendCmd("AT+COPS?");
  //wait_for("OK",2000);

  // Supprime tous les SMS présents au démarrage
  sendCmd("AT+CMGD=1,4");
  //wait_for("OK",2000);
  
  // SMS en mode texte
  sendCmd("AT+CMGF=1");
  //wait_for("OK",2000);
  
  // Pour recevoir les SMS via +CMTI:...
  sendCmd("AT+CNMI=2,1,0,0,0"); 
  //wait_for("OK",2000);
}
 
void loop()
{
    char SerialInByte;
     
    if(Serial.available())
    {
       SIM900.print((unsigned char)Serial.read());
     }  
    else  if(SIM900.available())
    {
        char SerialInByte;
        SerialInByte = (unsigned char)SIM900.read();
        
        // EN: Relay to Arduino IDE Monitor
        // FR: Relayer l'information vers le moniteur Serie Arduino
        Serial.print( SerialInByte );
        
        // -------------------------------------------------------------------
        // EN: Program also listen to the GPRS shield message.
        // FR: Le programme écoute également les messages issus du GPRS Shield.
        // -------------------------------------------------------------------
        
        // EN: If the message ends with <CR> then process the message
        // FR: Si le message se termine par un <CR> alors traiter le message 
        if( SerialInByte == 13 ){
          // EN: Store the char into the message buffer
          // FR: Stocké le caractère dans le buffer de message
          if (msg.length()>1)
            ProcessGprsMsg();
         }
         if( SerialInByte == 10 ){
            // EN: Skip Line feed
            // FR: Ignorer les Line Feed 
         }
         else {
           // EN: store the current character in the message string buffer
           // FR: stocker le caractère dans la mémoire tampon réservé au message
           msg += String(SerialInByte);
         }
     }   
}

// EN: Make action based on the content of the SMS. 
//     Notice than SMS content is the result of the processing of several GPRS shield messages.
// FR: Execute une action sur base du contenu d'un SMS.
//     Notez que le contenu du SMS est le résultat du traitement de plusieurs messages du shield GPRS.
void ProcessSms( String sms ){
  Serial.print( "ProcessSms for [" );
  Serial.print( sms );
  Serial.println( "]" );
  
  if( sms.indexOf("on") >= 0 ){
    digitalWrite( ledPin, HIGH );
    Serial.println( "LED IS ON" );
    return;
  }
  if( sms.indexOf("off") >= 0 ){
    digitalWrite( ledPin, LOW );
    Serial.println( "LED IS OFF" );
    return;
  }
}

// EN: Send the SIM PIN Code to the GPRS shield
// FR: Envoyer le code PIN de la carte SIM au shield GRPS
void GprsSendPinCode(){
  if( SIM_PIN_CODE.indexOf("XXXX")>=0 ){
    Serial.println( "*** OUPS! you did not have provided a PIN CODE for your SIM CARD. ***" );
    Serial.println( "*** Please, define the SIM_PIN_CODE variable . ***" );
    return;
  }
  SIM900.print("AT+CPIN=");
  SIM900.println( SIM_PIN_CODE );
}

// EN: Request Text Mode for SMS messaging
// FR: Demande d'utiliser le mode Text pour la gestion des messages
void GprsTextModeSMS(){
  SIM900.println( "AT+CMGF=1" );
}

/* void GprsReadSmsStore( String SmsStorePos ){
  // Serial.print( "GprsReadSmsStore for storePos " );
  // Serial.println( SmsStorePos ); 
  SIM900.print( "AT+CMGR=" );
  SIM900.println( SmsStorePos );
}*/

// EN: Clear the GPRS shield message buffer
// FR: efface le contenu de la mémoire tampon des messages du GPRS shield.
void ClearGprsMsg(){
  msg = "";
}

// EN: interpret the GPRS shield message and act appropiately
// FR: interprete le message du GPRS shield et agit en conséquence
void ProcessGprsMsg() {
  Serial.println("");
  Serial.print( "GPRS Message: [" );
  Serial.print( msg );
  Serial.println( "]" );
  
  if( msg.indexOf( "+CPIN: SIM PIN" ) >= 0 ){
     Serial.println( "*** NEED FOR SIM PIN CODE ***" );
     Serial.println( "PIN CODE *** WILL BE SEND NOW" );
     GprsSendPinCode();
  }

  if( msg.indexOf( "Call Ready" ) >= 0 ){
     Serial.println( "*** GPRS Shield registered on Mobile Network ***" );
     GprsTextModeSMS();
  }
  
  // EN: unsolicited message received when getting a SMS message
  // FR: Message non sollicité quand un SMS arrive
  if( msg.indexOf( "+CMTI" ) >= 0 ){
     Serial.println( "*** SMS Received ***" );
     // EN: Look for the coma in the full message (+CMTI: "SM",6)
     //     In the sample, the SMS is stored at position 6
     // FR: Rechercher la position de la virgule dans le message complet (+CMTI: "SM",6) 
     //     Dans l'exemple, le SMS est stocké à la position 6
     int iPos = msg.indexOf( "," );
     String SmsStorePos = msg.substring( iPos+1 );
     Serial.print( "SMS stored at " );
     Serial.println( SmsStorePos );
     
     // EN: Ask to read the SMS store
     // FR: Demande de lecture du stockage SMS
     SIM900.print( "AT+CMGR=" );
     SIM900.println( SmsStorePos );

     //GprsReadSmsStore( SmsStorePos );
  }
  
  // EN: SMS store readed through UART (result of GprsReadSmsStore request)  
  // FR: Lecture du stockage SMS via l'UART (résultat de la requete GprsReadSmsStore)
  if( msg.indexOf( "+CMGR:" ) >= 0 ){
    // EN: Next message will contains the BODY of SMS
    // FR: Le prochain message contiendra le contenu du SMS
    SmsContentFlag = 1;
    // EN: Following lines are essentiel to not clear the flag!
    // FR: Les ligne suivantes sont essentielle pour ne pas effacer le flag!
    ClearGprsMsg();
    return;
  }
  
  // EN: +CMGR message just before indicate that the following GRPS Shield message 
  //     (this message) will contains the SMS body
  // FR: le message +CMGR précédent indiquait que le message suivant du Shield GPRS 
  //     (ce message) contient le corps du SMS 
  if( SmsContentFlag == 1 ){
    Serial.println( "*** SMS MESSAGE CONTENT ***" );
    Serial.println( msg );
    Serial.println( "*** END OF SMS MESSAGE ***" );
    ProcessSms( msg );
  }
  
  ClearGprsMsg();
  // EN: Always clear the flag
  // FR: Toujours mettre le flag à 0
  SmsContentFlag = 0; 
}

void sendCmd(String cmd)
{
  //Serial.println(cmd);
  SIM900.println(cmd);  //Close TCP Connection
  //delay(2000); 
  //while(!SIM900.available()) {delay(100);}

  while(SIM900.available()!=0)
    Serial.write(SIM900.read());
    
  //Serial.println("---------------------------");
}

